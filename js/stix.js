//HashMap auxiliar

function HashMap(hash) {
    let map = new Map();
    let _set = map.set;
    let _get = map.get;
    let _has = map.has;
    let _delete = map.delete;
    map.set = function (k, v) {
        return _set.call(map, hash(k), v);
    };
    map.get = function (k) {
        return _get.call(map, hash(k));
    };
    map.has = function (k) {
        return _has.call(map, hash(k));
    };
    map.delete = function (k) {
        return _delete.call(map, hash(k));
    };
    return map;
}

Grid = {
    //cuanto se movio en x y en y restarte al borde
    pintar_region: function (x, y, border) {
        for (let i = x - border; i < x + border; i++) {
            for (let j = y - border; j < y + border; j++) {
                if (i >= 0 && i <= this.w && j >= 0 && j <= this.h) {
                    this.posicion.set([i, j], 1);
                }
            }
        }
    },

    offset: [50, 50],
    sparx: [],

    colores: [
        [0, 0, 0], // 0 vacio - negro
        [0, 0, 180], // 1 lleno
        [143, 0, 0], // 2 llenar
        [235, 230, 211], // 3 Color de Fondo
        [255, 192, 203], // 4 pink
        [255, 255, 255], // 5 TRACE
        [255, 255, 255], // 6 blanco
        [192, 192, 192], // 7 muro
        [255, 0, 0], // 8 rojo
        [255, 0, 0], // 9 rojo
        [255, 0, 0], // 10 rojo
        [255, 0, 255] // 11 
    ], 

    

    init: function () {
        this.canvas = document.getElementById('grid');
        this.canvas.width = 640;
        this.canvas.height = 480;

        this.ctx = this.canvas.getContext("2d"); //para representar lo que escribamos
        this.w = 194; // grid ancho
        this.h = 122; // grid alto
        this.width = this.w;
        this.height = this.h;

        // Inicializar las posiciones del grid a vacio 
        this._grid = [];
        for (let i = 0; i < this.w * (this.h + 2); i++) {
            this._grid[i] = 0;
        }

        // Inicializar un array de posiciones vacio
        this.recordarPosicion = [];
        for (let i = 0; i < this.w * (this.h + 2); i++) {
            this.recordarPosicion[i] = 0;
        }

        this.porcentaje = 0;
        this.area_llenada = 0;
        this.area_total = (this.w - 6) * (this.h - 6) / 4;
    },

    // Limpiar la pantalla
    limpiar: function () {
        this.ctx.fillStyle = "#000000";
        this.ctx.clearRect(Grid.offset[0], Grid.offset[1], this.w * 3, this.h * 3);
    },

    // Cambiar el valor de una posicion del grid
    set: function (x, y, v) {
        this._grid[(this.w * y) + x] = v;
        this.posicion.set([x, y], 1);
    },

    // Conseguir el valor de una posicion del grid
    get: function (x, y) {
        return this._grid[(this.w * y) + x];
    },

    // Reiniciar todos los valores del grid
    reset: function () {
        this.posicion = new HashMap(JSON.stringify);

        // Todos los valores vacios
        for (let x = 2; x < this.w; x++) {
            for (let y = 2; y < this.h; y++) {
                Grid.set(x, y, 0);
            }
        }

        let [w, h] = [this.w, this.h];
        
        // OUT
        for (let x = 0; x < w; x++) {
            Grid.set(x, 0, Player.OUT);
            Grid.set(x, 1, Player.OUT);
            Grid.set(x, h - 3, Player.OUT);
            Grid.set(x, h - 2, Player.OUT);
            Grid.set(x, h - 1, Player.OUT);
        }

        for (let y = 0; y < h; y++) {
            Grid.set(0, y, Player.OUT);
            Grid.set(1, y, Player.OUT);
            Grid.set(w - 3, y, Player.OUT);
            Grid.set(w - 2, y, Player.OUT);
            Grid.set(w - 1, y, Player.OUT);
        }

        // Muros
        for (let x = 2; x < w - 3; x++) {
            Grid.set(x, 2, Player.MURO);
            Grid.set(x, h - 4, Player.MURO);
        }

        for (let y = 2; y < h - 3; y++) {
            Grid.set(2, y, Player.MURO);
            Grid.set(w - 4, y, Player.MURO);
        }
        
    },

    blit: function (x, y, v) {},

    pintar: function () {
        this.posicion.forEach((value, key, map) => {
            let punto = JSON.parse(key);
            let x = punto[0];
            let y = punto[1];
            let v = Grid.get(x, y);

            this.ctx.fillStyle = 'rgb(' + this.colores[v].join(',') + ')';
            if ((Grid.offset[1] + y * 3) + 3 <= Grid.offset[1] + Grid.h * 3) {
                this.ctx.fillRect(Grid.offset[0] + x * 3, Grid.offset[1] + y * 3, 3, 3);
            }

        });

        Grid.posicion = new HashMap(JSON.stringify);
    },

    // Devuelve si la coordenada es una pared
    entorno_pared: function (x, y) {
        return (this.get(x, y) === Player.MURO);
    },

    // Devuelve si la coordenada esta vacia
    entorno_vacio: function (x, y) {
        return (this.get(x, y) === Player.VACIO);
    },

    // Cambia el valor v0 a v1 en el grid siguiendo la direccion de p0 a p1 (sin incluirlos)
    // Comienza a probar por la dirección dir. Devuelve la ultima direccion
    trazar: function (p0, p1, dir, v0, v1) {
        let [x, y] = p0;
        let [dx, dy] = dir;

        let turno = {
            "0,0": [0, 1],
            "1,0": [0, 1],
            "0,1": [-1, 0],
            "-1,0": [0, -1],
            "0,-1": [1, 0],
        };

        let found = false;
        while (!found) {
            if (x + dx === p1[0] && y + dy === p1[1]) {
                found = true;
            } else if (Grid.get(x + dx, y + dy) === v0) {
                Grid.set(x + dx, y + dy, v1);
                x += dx;
                y += dy;
            } else {
                [dx, dy] = turno[[dx, dy].join(",")];
            }
        }
        return [dx, dy];
    },

    // Rellena una region bordeada por vlist y conteniendo el punto p0, con el valor v
    fill: function (p0, vlist, v, invert) {
        let vf = this.vmap(vlist);
        let y_llenado = this.buscar(p0, [3, 3], vlist);
        if (invert) {
            y_llenado = !y_llenado;
        }
        let area = 0;
        let llenar;
        for (let y = 3; y < this.h - 3; y = y + 2) {
            llenar = y_llenado;
            for (let x = 3; x < this.w - 3; x++) {
                if (vf(x, y)) {
                    llenar = !llenar;
                } else {
                    if (llenar) {
                        Grid.set(x, y, v);

                        area += (x & 1); // sumar sólo impares
                        if (!vf(x, y + 1)) {
                            Grid.set(x, y + 1, v);
                        }
                    }
                }
            }
            if (vf(3, y + 1)) {
                y_llenado = !y_llenado;
            }
        }
        this.area_llenada += area;
        this.porcentaje = 100 * this.area_llenada / this.area_total;
        return area;
    },

    // Devuelve una funcion que dice si una coordenada contiene alguno de los valores de vset en el grid
    vmap: function (vset) {
        let a = [];
        for (let i = 0; i < 16; i++) {
            a.push(vset.includes(i));
        }
        let obj = this;

        function entorno_f(x, y) {
            if (y === undefined) {
                return a[x];
            } else if (y >= 0) {
                return a[obj._grid[obj.w * y + x]];
            } else {
                return 0;
            }
        }

        return entorno_f;
    },

    // Comprueba si la coordenada traceable
    entorno_traceable: function (x, y) {
        let vf = this.vmap([4, 7]);
        return vf(x, y);
    },

    

    // Comprueba si dos puntos se encuentran en la misma region limitada por vlist
    buscar: function (p0, p1, vlist) {
        let c = 0;
        let x1, x2, y1, y2;
        let encontrar = this.vmap(vlist);

        if (p0[0] <= p1[0]) {
            x1 = p0[0];
            y1 = p0[1];
            x2 = p1[0];
            y2 = p1[1];
        } else {
            x2 = p0[0];
            y2 = p0[1];
            x1 = p1[0];
            y1 = p1[1];
        }

        for (let i = x1; i <= x2; i++) {
            if (encontrar(i, y1)) {
                c++;
            }
        }

        if (y1 <= y2) {
            x1 = x2;
            // y1 se queda igual
        } else {
            let ytmp = y1;
            x1 = x2;
            y1 = y2;
            // x2 se queda igual
            y2 = ytmp;
        }

        for (let i = y1; i <= y2; i++) {
            if (encontrar(x1, i)) {
                c++;
            }
        }

        return (c % 2 === 0);
    },

    // Agrega un recordatorio en la posicion
    añadir_recordatorio: function (x, y) {
        this.recordarPosicion[(this.w * y) + x]++;
    },

    // Devuelve las posicion guardadas
    get_recordatorio: function (x, y) {
        return this.recordarPosicion[(this.w * y) + x];
    },

    // Dibuja una linea entre los dos puntos
    dibujarLinea: function (p0, p1) {
        this.ctx.beginPath();
        this.ctx.moveTo(p0[0], p0[1]);
        this.ctx.lineTo(p1[0], p1[1]);
        this.ctx.strokeStyle = 'rgb(255,0,0)';
        this.ctx.stroke();
    },

    // Muestra la puntuacion
    mostrarPuntaje: function () {
        this.ctx.fillStyle = 'rgb(0,255,0)';
        this.ctx.clearRect(0, 0, this.canvas.width - 3, Grid.offset[1] - 3);
        this.ctx.font = "32px Georgia";
        this.ctx.fillText("Puntuacion: " + Player.score, 80, 35);
        this.ctx.fillText("Area: " + Grid.porcentaje.toFixed(2) + " / 75%", 350, 35);
    },

    
};

//Espera un tiempo especificado (cuenta hacia atras)
function esperarTiempo(milisegundos) {
    return new Promise(resolve => setTimeout(resolve, milisegundos));
}

async function start() {
  
    document.addEventListener("keydown", Key.onKeydown);
    document.addEventListener("keyup", Key.onKeyup);
    //keydown y keyup nos indicaran cual es el codigo ASCII del caracter
    //que se presiono

    Grid.ctx.font = "30px Arial";
    Grid.ctx.fillStyle="white";
    Grid.ctx.clearRect(Grid.offset[0] + Grid.width * 1.4, Grid.offset[1] + Grid.height * 1.3, Grid.width / 5, Grid.height / 3);
    Grid.ctx.fillText("3", Grid.offset[0] + Grid.width * 1.5, Grid.offset[1] + Grid.height * 1.5);
    await esperarTiempo(1000);
    Grid.ctx.clearRect(Grid.offset[0] + Grid.width * 1.4, Grid.offset[1] + Grid.height * 1.3, Grid.width / 5, Grid.height / 3);
    Grid.ctx.fillText("2", Grid.offset[0] + Grid.width * 1.5, Grid.offset[1] + Grid.height * 1.5);
    await esperarTiempo(1000);
    Grid.ctx.clearRect(Grid.offset[0] + Grid.width * 1.4, Grid.offset[1] + Grid.height * 1.3, Grid.width / 5, Grid.height / 3);
    Grid.ctx.fillText("1", Grid.offset[0] + Grid.width * 1.5, Grid.offset[1] + Grid.height * 1.5);
    await esperarTiempo(1000);

    game.mainLoop();
}

function pausa() {
    if (game.pausa) {
        game.pausa = false;
        game.mainLoop();
    } else {
        game.pausa = true;
    }
}

/*  llevará el estado de las teclas left, right, down, up y space */
estado_teclas = {};

Key = {

    disableAll: function () {
        estado_teclas.shift = false;
        estado_teclas.up = false;
        estado_teclas.down = false;
        estado_teclas.right = false;
        estado_teclas.left = false;
    },

    onKeydown: function (event) {
        Key.disableAll();
        // Tecla Shift
        estado_teclas.shift = event.shiftKey;
        // Flechas de dirección
        switch (event.keyCode) {
            case 37:
                event.preventDefault();
                estado_teclas.left = true;
                break;
            case 38:
                event.preventDefault();
                estado_teclas.up = true;
                break;
            case 39:
                event.preventDefault();
                estado_teclas.right = true;
                break;
            case 40:
                event.preventDefault();
                estado_teclas.down = true;
                break;
            case 112:
                pausa();
                break;
            case 80:
                pausa();
                break;
            default:
        }
    },

    onKeyup: function (event) {
        switch (event.keyCode) {
            case 37:
                event.preventDefault();
                estado_teclas.left = false;
                break;
            case 38:
                event.preventDefault();
                estado_teclas.up = false;
                break;
            case 39:
                event.preventDefault();
                estado_teclas.right = false;
                break;
            case 40:
                event.preventDefault();
                estado_teclas.down = false;
                break;
            default:
        }
    }
};



game = {

    termino: false,
    gano: false,
    pausa: false,
    then: Date.now(),
    
    timer: 30,
    frameCount: 0,

    // Medir los FPS (y funciones agregadas)
    measureFPS: function () {
        let now = Date.now();
        this.frameCount++;
        this.delta = (now - this.then);
        if (this.delta > 1000) {
            this.fps = this.frameCount;
           
            this.frameCount = 0;
            this.then = now;
            this.timer--;
            if (this.timer <= 0) {
                Grid.sparx.push(new Sparx([Math.round(Grid.w / 2), 2], [1, 0]));
                Grid.sparx.push(new Sparx([Math.round(Grid.w / 2), 2], [-1, 0]));
                this.timer = 30;
            }
        }
    },

    cargarSparx: function () {

        resources.load([
            'res/img/sprites.png',
        ]);
        resources.onReady(() => {
            start();
        });
    },

    // Bucle principal del juego
    mainLoop: function () {

        if (!game.termino) {
            if (!game.pausa) {
                Grid.pintar();
                Player.update();
                Player.pintar();
                Grid.sparx.forEach(function (sparx) {
                    sparx.update();
                    sparx.pintar();
                });
                Qix.update();
                Qix.dibujarQ();
                game.measureFPS();
                Grid.mostrarPuntaje();
                
                requestAnimationFrame(game.mainLoop);
            }
        } else {
           

            if (game.gano) {
                

                Grid.ctx.font = "150px Georgia";
                Grid.ctx.strokeStyle = 'white';
                Grid.ctx.lineWidth = 8;
                Grid.ctx.strokeText("Victory!", 80, 280);
                Grid.ctx.fillStyle = 'white';
                Grid.ctx.fillText("Victory!", 80, 280);

            } else {
                

                Grid.ctx.font = "150px Georgia";
                Grid.ctx.strokeStyle = 'white';
                Grid.ctx.lineWidth = 8;
                Grid.ctx.strokeText("Game", 140, 200);
                Grid.ctx.strokeText("Over", 175, 350);
                Grid.ctx.fillStyle = 'white';
                Grid.ctx.fillText("Game", 140, 200);
                Grid.ctx.fillText("Over", 175, 350);
            }
        }
    }
};



Qix = {
    //comparar si x es o no es igual a y 
    _cmp: function (x, y) {
        if (x > y) return 1;
        else if (x < y) return -1;
        else return 0;
    },

    vector: function (p0, p1) {
        // devuelve un vector de movimiento en un espacio (en una de las 8 direcciones) de p0 a p1'.
        let [x0, y0] = p0;
        let [x1, y1] = p1;
        let ux = this._cmp(x1 - x0, 0);
        let uy = this._cmp(y1 - y0, 0);

        if (Math.abs(x1 - x0) > 2 * Math.abs(y1 - y0)) {
            return [ux, 0];
        }

        if (Math.abs(y1 - y0) > 2 * Math.abs(x1 - x0)) {
            return [0, uy];
        }

        return [ux, uy];
    },


    limpiar_linea: function (p0, p1) {
        // slope = (y2 - y1) / (x2 - x1)
        let m = (p1[1] - p0[1]) / (p1[0] - p0[0]);
        // intercept = y - m * x
        let b = p0[1] - m * p0[0];

        if (p0[0] === p1[0]) {
            let [y1, y2] = p0[1] < p1[1] ? [p0[1], p1[1]] : [p1[1], p0[1]];
            for (let y = y1; y <= y2; y++) {
                Grid.pintar_region(p0[0] - 1, y, 1);
                Grid.pintar_region(p0[0] + 1, y, 1);
            }
        } else {
            let [x1, x2] = p0[0] < p1[0] ? [p0[0], p1[0]] : [p1[0], p0[0]];
            for (let i = x1 - 1; i <= x2 + 1; i++) {
                let y = Math.round(m * i + b);
                Grid.pintar_region(i, y, 1);
                Grid.pintar_region(i, y + 2, 1);
                Grid.pintar_region(i, y - 2, 1);
            }

            let [y1, y2] = p0[1] < p1[1] ? [p0[1], p1[1]] : [p1[1], p0[1]];
            for (let i = y1 - 1; i <= y2 + 1; i++) {
                let x = Math.round((i - b) / m);
                Grid.pintar_region(x, i, 1);
                Grid.pintar_region(x + 2, i, 1);
                Grid.pintar_region(x - 2, i, 1);
            }
        }
    },

    left_turno: {
        "[0,1]": [1, 0],
        "[1,0]": [0, -1],
        "[0,-1]": [-1, 0],
        "[-1,0]": [0, 1]
    },

    right_turno: {
        "[0,1]": [-1, 0],
        "[1,0]": [0, 1],
        "[0,-1]": [1, 0],
        "[-1,0]": [0, -1]
    },

    _construct_: function () {
        this.vectors = [
            [1, -1],
            [1, 0],
            [1, 1],
            [0, 1],
            [-1, 1],
            [-1, 0],
            [-1, -1],
            [0, -1]
        ]; // movimientos del vector

        this.uvec_index = new HashMap(JSON.stringify);
        for (let i = 0; i < 8; i++)
            this.uvec_index.set(this.vectors[i], i);

        this.direcciones = [
            [1, -2],
            [2, -1],
            [2, 1],
            [1, 2],
            [-1, 2],
            [-2, 1],
            [-2, -1],
            [-1, -2]
        ]; // # enemigo movimientos
        this.dir_index = new HashMap(JSON.stringify);
        for (let i = 0; i < 8; i++)
            this.dir_index.set(this.direcciones[i], i);

        // # directional knight move operators
        this.dir_indica_x = new HashMap(JSON.stringify);
        this.direcciones.forEach(x => this.dir_indica_x.set(x, [-x[0], x[1]]));

        this.dir_indica_y = new HashMap(JSON.stringify);
        this.direcciones.forEach(x => this.dir_indica_y.set(x, [x[0], -x[1]]));

        //movimiento hacia el sentido de las agujas del reloj
        this.dir_reloj = new HashMap(JSON.stringify);
        this.direcciones.forEach(x => this.dir_reloj.set(x, this.direcciones[(this.dir_index.get(x) + 1) % 8]));

        this.dir_rreloj = new HashMap(JSON.stringify);
        this.direcciones.forEach(x => this.dir_rreloj.set(x, this.direcciones[(this.dir_index.get(x) + 7) % 8]));


        this.dir_approach = new HashMap(JSON.stringify);
        for (let i = 0; i < 8; i++) {
            for (let j = 0; j < 4; j++) {
                this.dir_approach.set([this.direcciones[i], this.vectors[(i + j) % 8]], this.dir_reloj.get(this.direcciones[i]));
                this.dir_approach.set([this.direcciones[i], this.vectors[(i + j + 4) % 8]], this.dir_rreloj.get(this.direcciones[i]));
            }
        }

        this.dir_pasos = new HashMap(JSON.stringify); 

        let dx, dy;
        let ux, uy;
        for (let i = 0; i < 8; i++) {
            [dx, dy] = this.direcciones[i];
            ux = this._cmp(dx, 0);
            uy = this._cmp(dy, 0);
            if (Math.abs(dx) > Math.abs(dy)) {
                this.dir_pasos.set([dx, dy], [
                    [ux, 0],
                    [0, uy],
                    [ux, 0]
                ]);
            } else {
                this.dir_pasos.set([dx, dy], [
                    [0, uy],
                    [ux, 0],
                    [0, uy]
                ]);
            }
        }
    }, 

    _init_: function () {
        this.p = [ //posicion
            [55, 55],
            [57, 59]
        ]; 
        this.d = [ //direccion enemigo
            [1, 2],
            [2, 1]
        ]; 
        this.history = [];
        this.dhistory = [];
        this.fase = 0; 
    },

    pscan: function (p0, p1) {
        // buscar el cruce de líneas desde los puntos impares p0 a p1; devolver el primer punto de cruce y la dirección
        let g = Grid;
        let [x, y] = p0;
        let [x0, y0] = p0;
        let [x1, y1] = p1;
        let ux = this._cmp(x1, x0);
        let uy = this._cmp(y1, y0);
        let adx = Math.abs(x1 - x0) + 1;
        let ady = Math.abs(y1 - y0) + 1;

        while (x !== x1 || y !== y1) {
            if (Math.abs((x1 - x) * ady) > Math.abs((y1 - y) * adx)) {
                if (!g.entorno_vacio((x + ux), y)) {
                    return [
                        [x + ux, y],
                        [ux, 0]
                    ];
                } else {
                    x += 2 * ux;
                }
            } else {
                if (!g.entorno_vacio(x, (y + uy))) {
                    return [
                        [x, y + uy],
                        [0, uy]
                    ];
                } else {
                    y += 2 * uy;
                }
            }
        }
        return null;
    },

    trazar_carrera: function (p0, d0, p1, d1) {
        
        let g = Grid;
        let [x0, y0] = p0;
        let [x1, y1] = p1;
        let [dx0, dy0] = d0;
        let [dx1, dy1] = d1;
        while (true) {
            if (!g.entorno_pared(x0 + dx0, y0 + dy0)) {
                if (g.entorno_pared(x0 + dy0, y0 + dx0)) {
                    [dx0, dy0] = [dy0, dx0];
                } else if (g.entorno_pared(x0 - dy0, y0 - dx0)) {
                    [dx0, dy0] = [-dy0, -dx0];
                } else {
                    throw new Error('trace_race failed 1');
                }
            }
            x0 += dx0;
            y0 += dy0;
            if (x0 === p1[0] && y0 === p1[1]) {
                return [d0, [-d1[0], -d1[1]]];
            }
            if (!g.entorno_pared(x1 + dx1, y1 + dy1)) {
                if (g.entorno_pared(x1 + dy1, y1 + dx1)) {
                    [dx1, dy1] = [dy1, dx1];
                } else if (g.entorno_pared(x1 - dy1, y1 - dx1)) {
                    [dx1, dy1] = [-dy1, -dx1];
                } else {
                    throw new Error('trace_race failed 2');
                }
            }
            x1 += dx1;
            y1 += dy1;
            if (x1 === p0[0] && y1 === p0[1]) {
                return [
                    [-d0[0], -d0[1]], d1
                ];
            }
        }
    },

    //  aleatorio
    movimiento_random: function (i) {
        if (Math.random() > 0.5) {
            this.d[i] = this.dir_reloj.get(this.d[i]);
        } else {
            this.d[i] = this.dir_rreloj.get(this.d[i]);
        }
    },

    // Actualizamos la posicion del Qix
    update: function () {
        let g = Grid;
        // Aquí habrá que devolver el control si el Player acaba de pasar de nivel
        //    return

        let pg = [];
        let dg = [];

        let xoff = Grid.offset[0],
            yoff = Grid.offset[1]; // g.offset;
        let x, y, steps, dx, dy, px, py;

        for (let i = 0; i < 2; i++) {
            if (this.fase === 0) {
                //fase 0: elegir la dirección, dar los dos primeros pasos y ajustar el eje menor
                this.movimiento_random(i);
                // Crear más estrategias elegir una en cada paso del bucle)

                [x, y] = this.p[i];
                steps = this.dir_pasos.get(this.d[i]);

                // # step 0
                [dx, dy] = steps[0];
                if (g.entorno_pared(x + dx, y + dy)) {
                    if (dx !== 0) {
                        this.d[i] = this.dir_indica_x.get(this.d[i]);
                    } else {
                        this.d[i] = this.dir_indica_y.get(this.d[i]);
                    }
                } else {
                    x += 2 * dx;
                    y += 2 * dy;
                }
                // step 1
                [dx, dy] = steps[1];
                if (g.entorno_pared(x + dx, y + dy)) {
                    if (dx !== 0) {
                        this.d[i] = this.dir_indica_x.get(this.d[i]);
                    } else {
                        this.d[i] = this.dir_indica_y.get(this.d[i]);
                    }
                } else {
                    x += 2 * dx;
                    y += 2 * dy;
                }

                pg.push([xoff + (x - dx) * 3, yoff + (y - dy) * 3]);
                this.p[i] = [x, y];
                dg.push([x, y]);
            } else {
                // # phase 1: take last step
                [x, y] = this.p[i];
                steps = this.dir_pasos.get(this.d[i]);

                // # step 2
                [dx, dy] = steps[2];
                if (g.entorno_pared(x + dx, y + dy)) {
                    if (dx !== 0) {
                        this.d[i] = this.dir_indica_x.get(this.d[i]);
                    } else {
                        this.d[i] = this.dir_indica_y.get(this.d[i]);
                    }
                } else {
                    x += 2 * dx;
                    y += 2 * dy;
                }

                pg.push([xoff + x * 3, yoff + y * 3]);
                dg.push([x, y]);
                this.p[i] = [x, y];
            }
        }

        let hit0 = this.pscan(this.p[0], this.p[1]);
        let hit1 = this.pscan(this.p[1], this.p[0]);

        if (hit0) {
            let [p0, u0] = hit0;
            if (Grid.get(p0[0], p0[1]) === Player.RUTA || Grid.get(p0[0] + u0[0], p0[1] + u0[1]) === Player.RUTA) {
                game.termino = true;
            } else {
                if (hit1) {
                    let [p1, u1] = hit1;
                    if (Grid.get(p1[0], p1[1]) === Player.RUTA || Grid.get(p1[0] + u1[0], p1[1] + u1[1]) === Player.RUTA) {
                        game.termino = true;
                    } else {
                        let [d0, d1] = Qix.trazar_carrera(p0, [Qix.right_turno["[" + u0[0] + "," + u0[1] + "]"][0], Qix.right_turno["[" + u0[0] + "," + u0[1] + "]"][1]],
                            p1, [Qix.right_turno["[" + u1[0] + "," + u1[1] + "]"][0], Qix.right_turno["[" + u1[0] + "," + u1[1] + "]"][1]]);
                        this.d[0] = this.dir_approach.get([this.d[0], d0]);
                        this.d[1] = this.dir_approach.get([this.d[1], d1]);
                    }
                }
            }
        }

        this.dhistory.push(dg);
        this.dhistory = this.dhistory.slice(-8);

        this.history.push(pg);
        this.history = this.history.slice(-8);
        this.fase ^= 1;
    }, 

    // Dibuja el Qix
    dibujarQ: function () {

        for (let h = this.history, dh = this.dhistory, i = 0; i < h.length; i++) {
            Grid.dibujarLinea(h[i][0], h[i][1]);
            Qix.limpiar_linea(dh[i][0], dh[i][1]);
        }
    },
};

class Sparx {

    constructor(posicion, dir) {
        this.sprite = new Sprite('res/img/sprites.png', [0, 0], [16, 16], 1, [0, 1, 2, 3, 4, 5, 6, 7]);
        this.ctx = Grid.ctx;
        this.dir = dir;
        this.posicion = posicion;
        if (dir[0] === 1) { 
            this.turn = [Qix.left_turno, Qix.right_turno];
        } else { 
            this.turn = [Qix.right_turno, Qix.left_turno];
        }
        let [x0, y0] = [0, 0];
        let [x, y] = posicion;
    }

    // Movimiento del sparx
    update() {
        let [x0, y0] = [0, 0];
        let [x, y] = this.posicion;
        let [dx, dy] = this.dir;
        let g = Grid;
        let c = 0;

        if (g.get(x, y) === Player.MURO) {
            if (g.get(x + dx, y + dy) !== Player.MURO) {
                let valido = false;
                let dxtmp = dx;
                let dytmp = dy;
                let vueltas = 0;
                while (!valido || vueltas > 4) {
                    [dxtmp, dytmp] = [this.turn[1]["[" + dxtmp + "," + dytmp + "]"][0],
                        this.turn[1]["[" + dxtmp + "," + dytmp + "]"][1]
                    ];
                    if (g.get(x + dxtmp, y + dytmp) === Player.MURO && (x + dxtmp !== x - dx && y + dytmp !== y - dy)) {
                        dx = dxtmp;
                        dy = dytmp;
                        valido = true;
                    } else {
                        vueltas++;
                    }
                }
                if (!valido && vueltas > 4) {
                    dx = dx * -1;
                    dy = dy * -1;
                }
            }
            x += dx;
            y += dy;
        } else {
            if (g.get(x + dx, y + dy) === Player.MURO) {
                x += dx;
                y += dy;
                [dx, dy] = [this.turn[0]["[" + dx + "," + dy + "]"][0], this.turn[0]["[" + dx + "," + dy + "]"][1]];
            } else {
                Grid.añadir_recordatorio(x, y);
                let opciones = [];
                opciones.push([this.turn[0]["[" + dx + "," + dy + "]"][0], this.turn[0]["[" + dx + "," + dy + "]"][1]]);
                opciones.push([dx, dy]);
                opciones.push([this.turn[1]["[" + dx + "," + dy + "]"][0], this.turn[1]["[" + dx + "," + dy + "]"][1]]);
                let mejorGiro = [Number.MAX_SAFE_INTEGER, [0, 0]];
                opciones.forEach(function (giro) {
                    if (Grid.entorno_traceable(x + giro[0], y + giro[1])) {
                        if (Grid.get_recordatorio(x + giro[0], y + giro[1]) <= mejorGiro[0]) {
                            mejorGiro[0] = Grid.get_recordatorio(x + giro[0], y + giro[1]);
                            mejorGiro[1] = giro;
                        }
                    }
                });
                [dx, dy] = mejorGiro[1];
                x += dx;
                y += dy;
            }
        }

        if ((x === Player.posicion[0] && y === Player.posicion[1]) || (this.posicion[0] === Player.posicion[0] &&
                this.posicion[1] === Player.posicion[1])) {
            game.termino = true;
        }

        this.posicion = [x, y];
        Grid.pintar_region(x, y, 4);
        this.dir = [dx, dy];
        this.sprite.update(1);
    }

    pintar() {
        this.ctx.save();
        this.ctx.translate(Grid.offset[0] + this.posicion[0] * 3 - 6, Grid.offset[1] + this.posicion[1] * 3 - 6);
        this.sprite.render(this.ctx);
        this.ctx.restore();
    }
}

Player = {

    VACIO: 0, 
    RUTA: 5,

    MURO: 7,
    OUT: 3,

    TRAZARRUTA: 10,
    WALLTRACE: 11,

    FillScore: [0, 5, 10],
    add_score: function (n) {
        this.score += n;
    },

    // Inicializacion de variables
    init: function (pos) {
        this.posicion = pos;
        this.ctx = Grid.ctx;
        this.dx = 0;
        this.dy = 0;
        this.dir = [this.dx, this.dy];
        this.dibujar = 0;
        this.dibujando = 0;
        this.launch_point = null;
        this.score = 0;
    },

    // Movimiento del Player
    update: function () {
        let [x, y] = this.posicion;

        if (((x + y) & 1) === 0) { // Suma par, se puede girar
            [dx, dy, dibujar] = this.calc_dir();

            if (this.dibujando) {
                if (Grid.entorno_vacio(x + dx * 2, y + dy * 2) || Grid.entorno_pared(x + dx * 2, y + dy * 2) &&
                    ((x + dx * 2) !== this.launch_point[0] || (y + dy * 2) !== this.launch_point[1])) {
                    this.dir = [dx, dy];
                    x += dx;
                    y += dy;
                    Grid.set(x, y, Player.RUTA);
                    Player.posicion = [x, y];
                } else {
                    this.dir = [0, 0];
                }

            } else {
                if (Grid.entorno_pared(x + dx, y + dy)) {
                    this.dir = [dx, dy];
                    x += dx;
                    y += dy;
                    this.posicion = [x, y];
                } else if (dibujar && Grid.entorno_vacio(x + dx, y + dy) && /* Empezar a pintar */
                    Grid.entorno_vacio(x + dx * 2, y + dy * 2) || (dibujar && Grid.entorno_pared(x + dx * 2, y + dy * 2) && Grid.entorno_vacio(x + dx, y + dy))) {

                    this.launch_point = [x, y];
                    this.dir = [dx, dy];
                    x += dx;
                    y += dy;
                    this.posicion = [x, y];
                    this.dibujando = dibujar;
                    Grid.set(x, y, Player.RUTA);
                } else { // Direccion bloqueada
                    this.dir = [0, 0];
                }
            }
        } else { // Posicion impar, no se puede girar. Forzar a moverse a la siguiente casilla.
            [dx, dy] = this.dir;

            if (this.dibujando) {
                if (Grid.entorno_pared(x + dx, y + dy)) {
                    // landing
                    x += dx;
                    y += dy;

                    //  corner points
                    pa = [x - dx - dy, y - dy - dx];
                    pb = [x - dx + dy, y - dy + dx];

                    if (Grid.entorno_pared(x - dy, y - dx)) {
                        da = [-dy, -dx];
                    } else {
                        da = [dx, dy];
                    }
                    if (Grid.entorno_pared(x + dy, y + dx)) {
                        db = [+dy, +dx];
                    } else {
                        db = [dx, dy];
                    }
                    Grid.trazar([x, y], this.launch_point, [-dx, -dy], Player.RUTA, Player.TRAZARRUTA);
                    Grid.trazar([x, y], this.launch_point, da, Player.MURO, Player.WALLTRACE);

                    let test_point = Qix.p[0];
                    if (Grid.buscar(pa, test_point, [10, 7])) {
                        n = Grid.fill(pb, [10, 7], 2);

                        Player.add_score(n * Player.FillScore[1]);
                        if (Grid.porcentaje > 75) {
                            Player.add_score(Math.round(Grid.porcentaje - 75) * Player.FillScore[2]);
                            game.gano = true;
                            game.termino = true;
                        } else if (Grid.porcentaje === 75) {
                            game.gano = true;
                            game.termino = true;
                        }

                        Grid.current_filling = n;
                        Grid.trazar([x, y], this.launch_point, da, 11, Player.MURO); // # rewall
                        Grid.trazar([x, y], this.launch_point, db, Player.MURO, 4); // # bury
                        Grid.trazar([x, y], this.launch_point, [-dx, -dy], 10, Player.MURO); // # solidify
                    } else {
                        let [xa, ya] = this.launch_point;
                        Grid.set(xa, ya, 11);
                        Grid.set(x, y, 11);
                        n = Grid.fill(pa, [10, 11], 2);
                        Grid.current_filling = n;

                        Player.add_score(n * Player.FillScore[1]);
                        if (Grid.porcentaje > 75) {
                            Player.add_score(Math.round(Grid.porcentaje - 75) * Player.FillScore[2]);
                            game.gano = true;
                            game.termino = true;
                        } else if (Grid.porcentaje === 75) {
                            game.gano = true;
                            game.termino = true;
                        }

                        Grid.set(xa, ya, 7);
                        Grid.set(x, y, 7);
                        Grid.trazar([x, y], this.launch_point, da, 11, 4); // # bury;
                        Grid.trazar([x, y], this.launch_point, [-dx, -dy], 10, 7); //  # solidify;
                    }

                    this.dibujando = 0;
                    this.launch_point = null;

                } else {
                    x += dx;
                    y += dy;
                    Grid.set(x, y, Player.RUTA);
                }
            } else {
                x += dx;
                y += dy;
            }
        }

        this.posicion = [x, y];
        Grid.pintar_region(x, y, 3);
    },

    // Pintar al Player
    pintar: function () {
        this.ctx.beginPath();
        this.ctx.strokeStyle = "black";
        this.ctx.arc(Grid.offset[0] + (this.posicion[0] * 3), Grid.offset[1] + (this.posicion[1] * 3), 5, 0, 2 * Math.PI, true);
        this.ctx.stroke();
        this.ctx.fillStyle = "red";
        this.ctx.fill();
    },

    // Calcular la direccion que debe seguir segun las teclas
    calc_dir: function () {
        let dx = 0,
            dy = 0,
            dibujar = 0;

        if (estado_teclas.shift) {
            dibujar = 1;
        } else {
            dibujar = 0;
        }
        if (estado_teclas.left) {
            dx = -1;
            dy = 0;
        } else if (estado_teclas.right) {
            dx = 1;
            dy = 0;
        } else if (estado_teclas.up) {
            dx = 0;
            dy = -1;
        } else if (estado_teclas.down) {
            dx = 0;
            dy = 1;
        } else {
            dx = 0;
            dy = 0;
        }

        return [dx, dy, dibujar];
    },
};

Grid.init();
Grid.reset();
Qix._construct_();
Qix._init_();
Player.init([96, 118]);
Grid.sparx.push(new Sparx([Math.round(Grid.w / 2), 2], [1, 0]));
Grid.sparx.push(new Sparx([Math.round(Grid.w / 2), 2], [-1, 0]));

game.cargarSparx();